#!/bin/sh 
# "@(#) XX_FND_UTILS_INST.ksh : 12.2.9.01"
##############################################################################
#
#                 Copyright (c) 2022 Etat de Gen�ve
#                         All rights reserved
#
##############################################################################
#
# Application         : CFI
# Livrable            : XX_FND_UTILS
# Auteur              : Judica�l MORIS
# Date de cr�ation    : 24/02/2021
# Description         : Script d'installation des composants java
#
# Historique
# --------------
#
# Date         Auteur           Version   Description du changement
# ------------ ---------------- --------- -----------------------------------------
# 17.10.2022   Judica�l MORIS   12.2.9.01 Version initiale
##############################################################################

####################################################################
# Appel script d'initialisation xx_initialize.ksh:
# 
#   => positionnement des variables suivantes:
#       XX_WORKING_DB
#       XX_APPS_USER
#       XX_APPS_PASSWORD
#       XX_SID
#
#   => initialisation des commandes d'install suivantes:
#       xx_copy
#       xx_unjar
#       xx_rmdir
#       xx_mkdir
#       xx_rmfile
#       xx_import_xml
#       xx_import_jpx
#       xx_javac_file
#       xx_javac_dir
#
####################################################################
. $XX_FND_TOP/bin/xx_initialize.ksh

##############################
# Actions d'install
##############################

##############################

# -------------------------
# Copie des composants OAF
# -------------------------
cp -r ./fnd/java/* $JAVA_TOP/xxcfi/ 
if [ $? -ne 0 ]; then
    echo "  ERREUR - lors de la copie des sources OAF vers $JAVA_TOP/xxcfi !"
	exit 1
fi

echo ""
echo "OK - Sources OAF copi�es"

# -------
# Compil java
# -------
$xx_javac_dir $JAVA_TOP/xxcfi/oracle/apps/xxfnd/util

# Fin OK
echo ""
echo "BUILD SUCCESSFUL"

