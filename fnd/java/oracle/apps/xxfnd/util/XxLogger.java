/*
# "@(#) XxLogger.java : 12.2.9.01"
####################################################################################
#
#                 Copyright (c) 2022 Etat de Gen�ve
#                         All rights reserved
#
####################################################################################
#
# Application         : CFI
# Livrable            : XX_FND_UTILS
# Auteur              : Judica�l MORIS
# Date de cr�ation    : 17.10.2022
# Description         : Classe pour g�rer la table de log sp�cifique
#
# Historique
# ++++++++++++++
#
# Date         Auteur           Version    Description du changement
# ++++++++++++ ++++++++++++++++ ++++++++++ +++++++++++++++++++++++++++++++++++++++++
# 17.10.2022   Judica�l Moris   12.2.9.01  Version initiale
####################################################################################
*/
package oracle.apps.xxfnd.util;

import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.server.OADBTransactionImpl;
import oracle.jdbc.OracleCallableStatement;
import oracle.apps.fnd.framework.OAException;

public class XxLogger {

    public static void log(OAApplicationModule am, String module, String texte) {

         // Recuperation d un acces a la base de donnees
         OADBTransactionImpl oaDbTransact = (OADBTransactionImpl)am.getOADBTransaction();
         String activerAide = oaDbTransact.getProfile("XX_FND_ACTIVER_LOG");        
         if(activerAide != null && activerAide.equals("Y"))
         {
             // Creation de la chaine de caracteres de l appel a proc basee
             StringBuffer str = new StringBuffer();
             str.append( " BEGIN ");
             str.append( " XXCFI_UTILS_PKG.LOGGER( ");
             str.append( "       MODULE         => :1,  ");
             str.append( "       TEXTE          => :2   ");
             str.append( "    ); ");
             str.append( " END; ");
             // Creation de l'appel de la proc�dure
             OracleCallableStatement oracleStatement = (OracleCallableStatement)oaDbTransact.createCallableStatement(str.toString(), 0);
             
             try
             {
                 //Enregistrement des param�tres de la proc�dure stock�e
                 oracleStatement.setString(1, module);
                 oracleStatement.setString(2, texte);
    
                 // Execution de la procedure stockee
                 oracleStatement.execute();
    
                 oracleStatement.close();
    
             }
             catch(Exception e)
             {
                 throw OAException.wrapperException(e);
             }
         }
    }

}
