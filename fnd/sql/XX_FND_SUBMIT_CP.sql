---# "@(#) XX_FND_SUBMIT_CP.sql : 1.1"
--##############################################################################
--#
--#                 Copyright (c) 2001 Etat de Genève
--#                         All rights reserved
--#
--##############################################################################
--#
--# Application         : CFI
--# Livrable            : XXCFI_IBY_PPR_UTILS.zip
--# Auteur              : L. Mazzei 
--# Date de création    : 15.11.2017
--# Description         : Ce script est appelé par le shell Unix XX_FND_SUBMIT_CP.sh
--#                       et permet d'exécuter un traitement simultané
--#                       
--#
--# A executer sour le User Oracle 
--#                     : apps 
--#
--# Historique
--# --------------
--#
--# Date         Auteur               Ver. Description du changement
--# ------------ ----------------    ---- -----------------------------------------
--# 15.11.2017   L. Mazzei            1.0  9986  - Version Initiale
--# 01.02.2018   L. Mazzei            1.1  10057 - Gestion parametres dynamiques
--##############################################################################
--
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET SERVEROUTPUT ON size 1000000;
set linesize 1000;
set verify OFF
DECLARE
 v_conc_request_id         NUMBER; -- request_id du Traitement lancé par le shell
 v_conc_request_id2        NUMBER; -- request_id du Traitement 'CTLM (CFI - Compte rendu du gestionnaire de traitements)'
 v_conc_request_id_parent  NUMBER;
 v_phase                   VARCHAR2(50);
 v_status                  VARCHAR2(50);
 v_dev_phase               VARCHAR2(50);
 v_dev_status              VARCHAR2(50);
 v_conc_phase_enfant       VARCHAR2(50);     
 v_status_enfant           VARCHAR2(50);     
 v_dev_phase_enfant        VARCHAR2(50);     
 v_dev_status_enfant       VARCHAR2(50);   
 v_completion_text_enfant  VARCHAR2(500);    
 v_result_enfant           BOOLEAN;
 v_execute_code            BOOLEAN;
 v_list_lrid               VARCHAR2(4000); --contient la liste de tous les traitements 'fils' exécutés par le traitement principale
 v_etape                   VARCHAR2(4000); --permet en mode diagnostique d'affiche les étapes d'avancement du script sql
 v_information             VARCHAR2(4000); --en cas d'erreur, remonte le message de XXCFI_FND_CONC_PROGS_CTRL  
 --
 --variables spécifiques à ce traitement
 v_uname           VARCHAR2(500);
 v_pgr             VARCHAR2(500);
 v_prg_code_application VARCHAR2(100);
 v_rname           VARCHAR2(500);
 n_nb_param        NUMBER:=0; --Nombre de paramètres de lancement
 v_fileout         VARCHAR2(500);
 v_file_new_name   VARCHAR2(500);
 v_description     VARCHAR2(500);
 v_email_sortie    VARCHAR2(500);
 v_email_cc_sortie VARCHAR2(500);
 v_email_journal   VARCHAR2(500);
 v_uid             NUMBER;
 v_rid             NUMBER;
 v_aid             NUMBER;
 v_racd            VARCHAR2(50); --code application de la responsabilité
 v_P1              VARCHAR2(500);       v_P2           VARCHAR2(500);    v_P3              VARCHAR2(500);       v_P4           VARCHAR2(500);
 v_P5              VARCHAR2(500);       v_P6           VARCHAR2(500);    v_P7              VARCHAR2(500);       v_P8           VARCHAR2(500);
 v_P9              VARCHAR2(500);       v_P10          VARCHAR2(500);    v_P11             VARCHAR2(500);       v_P12          VARCHAR2(500);
 v_P13             VARCHAR2(500);       v_P14          VARCHAR2(500);    v_P15             VARCHAR2(500);       v_P16          VARCHAR2(500);
 v_P17             VARCHAR2(500);       v_P18          VARCHAR2(500);    v_P19             VARCHAR2(500);       v_P20          VARCHAR2(500);
 v_P21             VARCHAR2(500);       v_P22          VARCHAR2(500);    v_P23             VARCHAR2(500);       v_P24          VARCHAR2(500);
 v_P25             VARCHAR2(500);       v_P26          VARCHAR2(500);    v_P27             VARCHAR2(500);       v_P28          VARCHAR2(500);
 v_P29             VARCHAR2(500);       v_P30          VARCHAR2(500);
 -- 
 v_tmp             VARCHAR2(4000);
 v_test2           VARCHAR2(4000);
 v_res2            VARCHAR2(4000);
 --
 v_resultat        VARCHAR2 (4000); --chaine contenant le resultat du bloc PL/SQL
 v_taille_totale   NUMBER:=0; --nombre de caractères a afficher (pbm limite à 255 c.)
 v_nb_iter         NUMBER:=0; --nombre d'itérations a effectuer pour gérer l'affichage
 v_trunc_de        NUMBER;
 ii                NUMBER;
 --
 EX_PBM_INSERT     Exception; --Exception levée lors s'un probleme d'insertion
 --
BEGIN
 DBMS_OUTPUT.ENABLE(1000000);
 --dbms_output.put_line('Début du bloc PL/SQL.');
 IF (NVL('&2','X')='DEBUG') THEN     dbms_output.put_line('DEBUG'||v_etape);     END IF;
 --
 v_etape := 'Debut requete SQL (recuperation parametres globaux) '||'&1';
 IF (NVL('&2','X')='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
 --
 --
 SELECT XXCFI_UTILS_PKG.get_mapping('&1','0_PROGRAM')
       ,XXCFI_UTILS_PKG.get_mapping('&1','0_CODE_APPLICATION')
       ,XXCFI_UTILS_PKG.get_mapping('&1','1_USER_NAME')
       ,XXCFI_UTILS_PKG.get_mapping('&1','2_RESP_NAME')
       ,XXCFI_UTILS_PKG.get_mapping('&1','3_DEPL_FICHIER_OUT')
       ,XXCFI_UTILS_PKG.get_mapping('&1','4_NOM_FICHIER_DEPLACE') 
       ,XXCFI_UTILS_PKG.get_mapping('&1','9_EMAIL_SORTIE') 
       ,XXCFI_UTILS_PKG.get_mapping('&1','9_EMAIL_CC_SORTIE') 
       ,XXCFI_UTILS_PKG.get_mapping('&1','9_EMAIL_JOURNAL') 
       ,XXCFI_UTILS_PKG.get_mapping_description('&1')
 INTO v_pgr
     ,v_prg_code_application
     ,v_uname
     ,v_rname
	 ,v_fileout
	 ,v_file_new_name 
	 ,v_email_sortie
	 ,v_email_cc_sortie
	 ,v_email_journal
	 ,v_description
 FROM DUAL;
 --
 IF (NVL('&2','X')='DEBUG') THEN     
    dbms_output.put_line('***DEBUG*** shell Unix : P1= '||'&1');  
    dbms_output.put_line('***DEBUG*** shell Unix : P2= '||'&2');  
    dbms_output.put_line('***DEBUG*** shell Unix : P3= -'||'&3'||'-');  
    dbms_output.put_line('***DEBUG*** shell Unix : P4= -'||'&4'||'-');  
    dbms_output.put_line('***DEBUG*** v_pgr = '||v_pgr);  
    dbms_output.put_line('***DEBUG*** v_prg_code_application = '||v_prg_code_application);    
    dbms_output.put_line('***DEBUG*** v_uname = '||v_uname);    
    dbms_output.put_line('***DEBUG*** v_rname = '||v_rname);    
    dbms_output.put_line('***DEBUG*** v_fileout = '||v_fileout);    
    dbms_output.put_line('***DEBUG*** v_file_new_name = '||v_file_new_name);    
    dbms_output.put_line('***DEBUG*** v_email_sortie = '||v_email_sortie);    
    dbms_output.put_line('***DEBUG*** v_email_cc_sortie = '||v_email_cc_sortie);    
    dbms_output.put_line('***DEBUG*** v_email_journal = '||v_email_journal);    
    dbms_output.put_line('***DEBUG*** v_description = '||v_description);       
 END IF;
 --
 --Contextualisation
 --
 --
 v_etape := 'Récupération info de contextualisation: '||v_rname||'-'||v_uname;
 IF (NVL('&2','X')='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
 --
 SELECT u.user_id
       ,r.responsibility_id 
	   ,r.application_id
	   ,a.Application_Short_Name 
 INTO v_uid
     ,v_rid
     ,v_aid  
     ,v_racd	
 FROM fnd_responsibility   r,
      fnd_responsibility_tl rl,
      fnd_user u,
      fnd_application a
 WHERE r.responsibility_id = rl.responsibility_id
 AND rl.language = 'F'
 AND rl.responsibility_name = v_rname
 AND u.user_name = UPPER(v_uname)
 AND r.application_id = a.application_id;
 --
 --
 --
 v_etape := 'Info de contextualisation récupérée: '||v_uid||'-'||v_rid||'-'||v_aid;
 IF (NVL('&2','X')='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
 FND_GLOBAL.apps_initialize(v_uid,v_rid,v_aid);
 -- 
 --
 --
 v_etape := 'Recuperation parametres de lancement du traitement';
 IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
 --
 --Si le paramètre '$3' et '$4' sont renseignés, alors le programme doit supplanter la valeur du param '$3' par la valeur '$4'
 --Pour faciliter la lecture, seule le détail de récupération/supplantation du 1er paramètre est détaillé en mode diagnostique
IF (XXCFI_UTILS_PKG.get_mapping('&1','P1')<>'#ERR') THEN 
     IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||'récuperation P1='||v_P1);     END IF;
     IF ('&3'='P1') THEN 
       v_P1:='&4'; 
       IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||'supplantation v_P1='||v_P1);     END IF;
     ELSE
     IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||'récuperation P1');     END IF;
     EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P1')||' from dual' into v_P1; 
       IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||' P1: pas de supplantation ');     END IF;
     END IF;
     n_nb_param := n_nb_param+1;
   END IF;
--
IF (XXCFI_UTILS_PKG.get_mapping('&1','P2')<>'#ERR') THEN  
     IF ('&3'='P2') THEN 
       v_P2:='&4'; 
	 ELSE	 
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P2')||' from dual' into v_P2; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P3')<>'#ERR') THEN
     IF ('&3'='P3') THEN 
       v_P3:='&4'; 
	 ELSE	 
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P3')||' from dual' into v_P3; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P4')<>'#ERR') THEN
     IF ('&3'='P4') THEN 
       v_P4:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P4')||' from dual' into v_P4; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P5')<>'#ERR') THEN
     IF ('&3'='P5') THEN 
       v_P5:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P5')||' from dual' into v_P5; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P6')<>'#ERR') THEN
     IF ('&3'='P6') THEN 
       v_P6:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P6')||' from dual' into v_P6; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P7')<>'#ERR') THEN
     IF ('&3'='P7') THEN 
       v_P7:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P7')||' from dual' into v_P7; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P8')<>'#ERR') THEN
     IF ('&3'='P6') THEN 
       v_P8:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P8')||' from dual' into v_P8; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P9')<>'#ERR') THEN
     IF ('&3'='P9') THEN 
       v_P9:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P9')||' from dual' into v_P9; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P10')<>'#ERR') THEN
     IF ('&3'='P10') THEN 
       v_P10:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P10')||' from dual' into v_P10; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P11')<>'#ERR') THEN
     IF ('&3'='P11') THEN 
       v_P11:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P11')||' from dual' into v_P11; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P12')<>'#ERR') THEN
     IF ('&3'='P12') THEN 
       v_P12:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P12')||' from dual' into v_P12; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P13')<>'#ERR') THEN
     IF ('&3'='P13') THEN 
       v_P13:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P13')||' from dual' into v_P13; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P14')<>'#ERR') THEN
     IF ('&3'='P14') THEN 
       v_P14:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P14')||' from dual' into v_P14; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P15')<>'#ERR') THEN
     IF ('&3'='P15') THEN 
       v_P15:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P15')||' from dual' into v_P15; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P16')<>'#ERR') THEN
     IF ('&3'='P16') THEN 
       v_P16:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P16')||' from dual' into v_P16; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P17')<>'#ERR') THEN
     IF ('&3'='P17') THEN 
       v_P17:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P17')||' from dual' into v_P17; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P18')<>'#ERR') THEN
     IF ('&3'='P18') THEN 
       v_P18:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P18')||' from dual' into v_P18; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P19')<>'#ERR') THEN
     IF ('&3'='P19') THEN 
       v_P19:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P19')||' from dual' into v_P19; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P20')<>'#ERR') THEN
     IF ('&3'='P20') THEN 
       v_P20:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P20')||' from dual' into v_P20; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P21')<>'#ERR') THEN
     IF ('&3'='P21') THEN 
       v_P21:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P21')||' from dual' into v_P21; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P22')<>'#ERR') THEN
     IF ('&3'='P22') THEN 
       v_P22:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P22')||' from dual' into v_P22; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P23')<>'#ERR') THEN
     IF ('&3'='P23') THEN 
       v_P23:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P23')||' from dual' into v_P23; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P24')<>'#ERR') THEN
     IF ('&3'='P24') THEN 
       v_P24:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P24')||' from dual' into v_P24; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P25')<>'#ERR') THEN
     IF ('&3'='P25') THEN 
       v_P25:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P25')||' from dual' into v_P25; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P26')<>'#ERR') THEN
     IF ('&3'='P26') THEN 
       v_P26:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P26')||' from dual' into v_P26; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P27')<>'#ERR') THEN
     IF ('&3'='P27') THEN 
       v_P27:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P27')||' from dual' into v_P27; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P28')<>'#ERR') THEN
     IF ('&3'='P28') THEN 
       v_P28:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P28')||' from dual' into v_P28; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P29')<>'#ERR') THEN
     IF ('&3'='P29') THEN 
       v_P29:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P29')||' from dual' into v_P29; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
IF (XXCFI_UTILS_PKG.get_mapping('&1','P30')<>'#ERR') THEN
     IF ('&3'='P30') THEN 
       v_P30:='&4'; 
	 ELSE
       EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('&1','P30')||' from dual' into v_P30; 
     END IF;
   n_nb_param := n_nb_param+1;
   END IF;
 -- 
 --
 v_etape := 'Insertion dans table intermediaire';
 IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
 --
 BEGIN
 SELECT XXCFI_FND_CONC_PROGS_CTRL_PKG.TABLE_INSERT (
    'N/A',                         --ctrlm_group_number
    'N/A',                         --ctrlm group key
    'XX_FND_SUBMIT_CP.sh',         --ctrlm group type
    TO_CHAR(SYSDATE,'DDMMYY'),     --ctrlm date
    SUBSTR('&1',1,20),                          --ctrlm job number
    '&4',                          --filename
    'N/A',                         --concurrent_program_type
    UPPER(v_racd),                 --code application de la responsabilité
    v_rname,                       --nom de la responsabilité
    UPPER(v_uname),                --user_name de lancement
    UPPER(v_prg_code_application), --Code application du traitement
    v_pgr,                         --Nom court du traitement simultané
    'CTLM',                        --Description du traitement
    '',
    n_nb_param,v_P1,v_P2,v_P3,v_P4,v_P5,v_P6,v_P7,v_P8,v_P9,v_P10,v_P11,v_P12,v_P13,v_P14,v_P15,v_P16,v_P17,v_P18,v_P19,v_P20,v_P21,v_P22,v_P23,v_P24,v_P25,v_P26,v_P27,v_P28,v_P29,v_P30
    ) 
 INTO v_conc_request_id
 FROM DUAL;
 EXCEPTION
   WHEN OTHERS THEN RAISE EX_PBM_INSERT;
 END;
 --
 commit;
 --
 --
 v_etape := 'Insertion dans table intermediaire partie 2';
 IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
 --
  IF (v_conc_request_id = 0)
  THEN
     v_etape := 'Insertion dans table intermediaire partie 2 : FAILED';
     dbms_output.put_line ('FAILED~~');
  ELSE
    v_etape := 'Insertion dans table intermediaire partie 2 : SUCCESS';
	--
    v_dev_status := 'INITIAL';  
    v_dev_phase := 'INITIAL';      
    --
    IF ('&2'!='DEBUG') THEN DBMS_OUTPUT.DISABLE;END IF;
	--
    v_tmp:=XXCFI_FND_CONC_PROGS_CTRL_PKG.RUN_GTS(v_conc_request_id);
	--
    IF ('&2'!='DEBUG') THEN DBMS_OUTPUT.ENABLE(1000000);END IF;	
	--
    IF ('&2'='DEBUG') THEN dbms_output.put_line('***DEBUG***'||v_etape);END IF;
	--
	--Gestion ATTENTE
    v_etape := 'Insertion dans table intermediaire partie 2 : WAIT (id='||v_conc_request_id||')';
    IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
    --
	WHILE v_res2 IS NULL LOOP
        v_etape := 'Insertion dans table intermediaire partie 2 : WAIT (id='||v_conc_request_id||') LOOP DEBUT';
	    SELECT substr(return_code,instr(return_code,'#',1,3)+1,6)
	          ,SUBSTR(TRANSLATE(return_code, 'éèàùôö', 'eeauoo' ),1,250)
			  ,SUBSTR(return_code,1,instr(return_code,'#')-1) --#XX V1.2.new
	    INTO v_test2
	       , v_res2
		   , v_conc_request_id_parent
	    FROM XXCFI_FND_CONC_PROGS_CTRL
	    WHERE conc_prog_ctrl_id = v_conc_request_id;
		--
		--
		IF (v_test2='ERREUR') THEN
                v_information:=SUBSTR(v_res2,1,500);
		END IF;
		--
	END LOOP;
	-- 
    -- wait pour tous les traitements enfants
    v_etape := 'Insertion dans table intermediaire partie 2 : WAIT CHILD';
    IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
    --
       FOR v_rec_child IN
           (SELECT fcr.request_id
                  , fcr.parent_request_id
                 -- , fcr.outfile_name
                 -- , fcr.logfile_name
             FROM   fnd_concurrent_requests     fcr
                  , fnd_concurrent_programs     fcp
             WHERE  fcr.program_application_id       = fcp.application_id
             AND    fcr.concurrent_program_id        = fcp.concurrent_program_id
             AND    NVL(fcr.request_type, 'P')       <> 'S' -- Exclusion du programme de lancement de phase FNDRSSTG
             START WITH fcr.request_id               = v_conc_request_id_parent
             CONNECT BY PRIOR fcr.request_id         = fcr.parent_request_id
             ORDER BY fcr.request_id DESC
           )
       LOOP                
         --    
         v_etape := 'Insertion dans table intermediaire partie 2 : WAIT CHILD LOOP';
         IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
		 --
         v_result_enfant:=FND_CONCURRENT.WAIT_FOR_REQUEST    
               (v_rec_child.request_id,                  
               20,                          
               0,                           
               v_conc_phase_enfant,                     
               v_status_enfant,                    
               v_dev_phase_enfant,                 
               v_dev_status_enfant,                
               v_completion_text_enfant);
         --
         v_list_lrid := v_rec_child.request_id||'/'||v_status_enfant||'~~'||v_list_lrid;
         --
       END LOOP;
      --
	  --
	  IF (v_email_sortie <> 'NA') AND (v_email_sortie is not null)
	  THEN
	    --
		--CFI - Compte rendu du gestionnaire de traitements  est utilisé pour envoyer des emails de synthèse.
	    v_etape := 'Exécution du traitement d''envoi d''email';
        IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
        --
        v_conc_request_id2 := Fnd_Request.submit_request( application => 'XXFND'
                                                         ,program     => 'XX_FND_LOG_METIER'
                                                         ,description => 'CTLM'
                                                         ,start_time  => NULL
                                                         ,sub_request => FALSE
                                                         ,argument1   => ''                        --niveau journalisation
                                                         ,argument2   => ''                        --Organisation
                                                         ,argument3   => ''                        --Application
                                                         ,argument4   => v_rid                     --Resp
                                                         ,argument5   => ''                        --Groupe de traitements
                                                         ,argument6   => ''                        --Demandeur
                                                         ,argument7   => ''                        --Statut
                                                         ,argument8   => ''                        --Date de
                                                         ,argument9   => ''                        --Date à
                                                         ,argument10  => v_email_sortie            --Email - à
                                                         ,argument11  => v_email_cc_sortie         --Email - cc
                                                         ,argument12  => ''                        --Email - cci
                                                         ,argument13  => v_conc_request_id_parent  --lrid
                                                        );
        COMMIT;
        --
	  END IF;
      --
	  v_etape := 'Fin requete SQL';
      IF ('&2'='DEBUG') THEN     dbms_output.put_line('***DEBUG***'||v_etape);     END IF;
	  --
	  v_resultat := 'SUCCESS'                        --[0] Resultat de la requete SQL
	     ||'~~'||SUBSTR(v_information,1,500)         --[2] Message d'information [pkg CTM]
	     ||'~~'||v_list_lrid                         --[4] LRID des traitements simultanés
	     ||'~~';
	  --
	  --
	  --
	  SELECT length(   'SUCCESS'                       
	                 ||'~~'||SUBSTR(v_information,1,244)              
	                 ||'~~'||v_list_lrid                   
	                 ||'~~')
	  INTO v_taille_totale
	  FROM DUAL;
	  --
	  v_nb_iter := ROUND(v_taille_totale/250)+1;
	  --
	  v_trunc_de := 1;
	  ii:=1;
	  WHILE ii <= 5 LOOP
	    dbms_output.put_line(SUBSTR(v_resultat,v_trunc_de,250));
	    v_trunc_de := v_trunc_de+250;
	    ii := ii+1;
	  END LOOP;
	--
  END IF;
  --
EXCEPTION
WHEN EX_PBM_INSERT THEN 
    dbms_output.put_line('Probleme XXCFI_FND_CONC_PROGS_CTRL_PKG.TABLE_INSERT'||'-'||TO_CHAR(SQLCODE)||'-'||sqlerrm);
WHEN OTHERS THEN  
    dbms_output.put_line('Error While Submitting Concurrent Request '||'-'||v_etape||'-'||TO_CHAR(SQLCODE)||'-'||sqlerrm);
END;
/
EXIT; 