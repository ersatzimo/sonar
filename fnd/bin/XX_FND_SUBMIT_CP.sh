#!/bin/sh
#
# "@(#) XX_FND_SUBMIT_CP.sh : 1.4"
#-------------------------------------------------------------------------------
# SCRIPT : XX_FND_SUBMIT_REQ.sh                    	 				        
#-------------------------------------------------------------------------------
#  Fonction             : 			
#  Auteur               : Ludovic Mazzei   					
#  Commentaires         : Shell permettant d'ex�cuter un traitement simultan� et de copier	
#                         la sortie g�n�r�e dans un chemin d�fini dans la table de mapping			
#  Parametres d'entr�e  :    $1 : Cl� de mapping				
#  Retour               :   0 - Success
#                           1 - Warning
#                           2 - Error
#                           3 - Unexpected error 	
#-------------------------------------------------------------------------------
#  HISTORIQUE DES VERSIONS							
#-------------------------------------------------------------------------------
#  Date         Version  Auteur 	           Description		
#  ----------   -------- -------------------   ---------------------------------	
#  03.07.2017   1.0      Ludovic Mazzei        9726 - Version initiale				
#  09.08.2017   1.1      Ludovic Mazzei        9726 - Gestion des emails en 'Cc'			
#  11.10.2017   1.2      Ludovic Mazzei        9942 - Initialisation des V_Px par '' +  Ajout mode debug + message d'erreur
#  15.11.2017   1.3      Ludovic Mazzei        9986 - D�port du script SQL dans XX_FND_SUBMIT_CP.sql + Gestion des longs messages d'erreur
#  01.02.2018   1.4      Ludovic Mazzei        10057- Gestion de param�tres 'dynamiques' (non issus de OeBS)
#-------------------------------------------------------------------------------

# Les variables de connexion sont initialis�es � partir des fichiers .profile et .profile_pwd
clear
#
return_code=0
#
#R�cup�ration du param�tre n�1 : Nom table de mapping
if test -z "$1"
then
  echo "Veuillez preciser la cle 'mapping' des parametres de lancement du traitement simultan�"
  exit 1
fi
#
P_cle_mapping=$1
#
#R�cup�ration du param�tre n�2 : Mode diagnostique (DEBUG/NO)
if test -z "$2"
then
  echo "Veuillez preciser le 2ieme param�tre (mode diagnostique [DEBUG/NO]) "
  exit 1
  else
     if [ "$2" == "DEBUG" ]
     then
       echo " "
       echo "*****************  MODE DIAGNOSTIQUE ACTIF  ********************"
       echo " "
     P_mode_debug=$2
     else
     P_mode_debug='NO'
     fi
fi
#
#
#R�cup�ration du param�tre n�3 [Facultatif]: Param�tre dynamique qui supplante celui d�fini dans la table de mapping
#if test -z "$3"
#then
#  echo "Veuillez preciser un param�tre dynamique (qui supplante celui defini dans la table de mapping)"
#  exit 1
#fi
#
if test -z "$3"
then
#echo " p3 is null"
P_dyn1_lib='NONE'
P_dyn1_val='NONE'
else
P_dyn1=$3
P_dyn1_lib=${P_dyn1%=*}
P_dyn1_val=${P_dyn1#*=}
fi
#
#
# Afichage en-t�te du programme
echo "+---------------------------------------------------------------+"
echo "|  XX_FND_SUBMIT_CP.sh                                          |"                                           
echo "|  Programme permettant d'executer un traitement simultane      |"
echo "+---------------------------------------------------------------+"
echo " Cle 'mapping'        :  ${P_cle_mapping}"
echo " Mode diagnostique    :  ${P_mode_debug}"
if test -n "$3"
then echo " Parametre dynamique  :  ${P_dyn1_lib}=${P_dyn1_val}"
fi
echo "+---------------------------------------------------------------+"
#
#
resultat=`sqlplus -S ${ORACTI_APPSUSR}@${INSTANCE} @$XX_FND_TOP/sql/XX_FND_SUBMIT_CP.sql ${P_cle_mapping} ${P_mode_debug} ${P_dyn1_lib} "${P_dyn1_val}"`


#export ORACLE_HOME=$IAS_ORACLE_HOME

if [ "$2" == "DEBUG" ]
then
  echo "***************** DEBUG : Contenu de la variable 'resultat' *****************";
  echo ">"$resultat;
  echo "*****************************************************************************";
fi

if (expr match "$resultat" 'SUCCESS') || (expr match "$resultat" 'DEBUG'); then 
  #
  echo "Traitement simultane execute."
  #
  #
  IFS="~~"
  set -A array $resultat
  echo " "
  echo "+---------------------------------------------------------------+"
  echo "         Informations provenant de l'application";
  echo "+---------------------------------------------------------------+"
  echo "${array[2]}"
  echo "+---------------------------------------------------------------+"
  echo "Statut du bloc PL/SQL           : "${array[0]}  #Resultat SQL SUCCESS/FAILURE
  echo "No. traitement principal        : "${array[4]}  #LRID du traitement principal
  i=6
  while [ $i -le 50 ]
  do  
    if [ -n "${array[$i]}" ];
    then print "No. traitement genere           : "${array[$i]}
    fi
    (( i=i+2 ))   
  done  
  echo "+---------------------------------------------------------------+"
else
  #
  echo "Erreur durant l'execution du bloc PL/SQL"
  echo "Message:"$resultat;
  #
fi
#
#Gestion du code retour pour Ctrl-M  
case ${array[4]#*/} in
  'Normal')
    return_code=0
    ;;
  'Avertissement')
    return_code=1 
    ;;
  'Erreur')
    return_code=2
    ;;
	*)
    return_code=3
    ;;
esac


echo "                                                         fin [${return_code}]."
echo "+---------------------------------------------------------------+"
return ${return_code}
