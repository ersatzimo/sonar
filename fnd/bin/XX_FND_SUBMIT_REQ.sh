#!/bin/sh
#
# "@(#) XX_FND_SUBMIT_REQ.sh : 1.1"
#-------------------------------------------------------------------------------
# SCRIPT : XX_FND_SUBMIT_REQ.sh                    	 				        
#-------------------------------------------------------------------------------
#  Fonction             : 			
#  Auteur               : Ludovic Mazzei   					
#  Commentaires         : Shell permettant d'ex�cuter une requete et de copier	
#                         la sortie g�n�r�e dans un chemin d�fini dans la table de mapping			
#  Parametres d'entr�e  :    $1 : Cl� de mapping				
#  Retour               : 	0 - Success
#                           1 - Warning
#                           2 - Error
#                           3 - Unexpected error 	
#-------------------------------------------------------------------------------
#  HISTORIQUE DES VERSIONS							
#-------------------------------------------------------------------------------
#  Date         Version  Auteur 	           Description		
#  ----------   -------- -------------------   ---------------------------------	
#  25.03.2017   1.0      Ludovic Mazzei        9726:Version initiale				
#  10.05.2017   1.1      Ludovic Mazzei        9726:Gestion renommage fichier de sortie
#-------------------------------------------------------------------------------

# Les variables de connexion sont initialis�es � partir des fichiers .profile et .profile_pwd
clear
#
return_code=0
#
if test -z "$1"
then
  echo "Veuillez preciser la cle 'mapping' des parametres de lancement de la requete"
  exit 1
fi
P_cle_mapping=$1
#
# Afichage en-t�te du programme
echo "+---------------------------------------------------------------+"
echo "|  XX_FND_SUBMIT_REQ.sh                                         |"                                           
echo "|  Programme permettant d'executer une requete                  |"
echo "+---------------------------------------------------------------+"
echo " Cle 'mapping' :  ${P_cle_mapping}"
echo "+---------------------------------------------------------------+"
#
#
#
resultat=`sqlplus -s ${ORACTI_APPSUSR}@${INSTANCE} <<EOF 
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET SERVEROUT ON;
DECLARE
 v_conc_request_id  NUMBER;
 v_phase            VARCHAR2(50);
 v_status           VARCHAR2(50);
 v_dev_phase        VARCHAR2(50);
 v_dev_status       VARCHAR2(50);
 v_execute_code     BOOLEAN;
 v_message          VARCHAR2(250);
 --
 --variables sp�cifiques � cette requete
 v_uname           VARCHAR2(500);
 v_pgr             VARCHAR2(500);
 v_rname           VARCHAR2(500);
 v_fileout         VARCHAR2(500);
 v_file_new_name   VARCHAR2(500);
 v_description     VARCHAR2(500);
 v_uid             NUMBER;
 v_rid             NUMBER;
 v_aid             NUMBER;
 v_P1              VARCHAR2(500);       v_P2           VARCHAR2(500);
 v_P3              VARCHAR2(500);       v_P4           VARCHAR2(500);
 v_P5              VARCHAR2(500);       v_P6           VARCHAR2(500);
 v_P7              VARCHAR2(500);       v_P8           VARCHAR2(500);
 v_P9              VARCHAR2(500);       v_P10          VARCHAR2(500);
 --
BEGIN
 --
 SELECT XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','0_PROGRAM')
       ,XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','1_USER_NAME')
       ,XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','2_RESP_NAME')
       ,XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','3_DEPL_FICHIER_OUT')
       ,XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','4_NOM_FICHIER_DEPLACE') --#V1.1 add
       ,XXCFI_UTILS_PKG.get_mapping_description('${P_cle_mapping}')
 INTO v_pgr
     ,v_uname
     ,v_rname
	 ,v_fileout
	 ,v_file_new_name --#V1.1 add
	 ,v_description
 FROM DUAL;
 --
 --Contextualisation
 --
 select u.user_id
       ,r.responsibility_id 
	   ,r.application_id
INTO v_uid
    ,v_rid
    ,v_aid   
 from fnd_responsibility   r,
      fnd_responsibility_tl rl,
      fnd_user u
 where r.responsibility_id = rl.responsibility_id
 and  rl.language = 'F'
 and  rl.responsibility_name = v_rname
 and  u.user_name = UPPER(v_uname);
 --
 --
 FND_GLOBAL.apps_initialize(v_uid,v_rid,v_aid);
 --
EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P1')||' from dual' into v_P1;
EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P2')||' from dual' into v_P2;
IF (XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P3')<>'#ERR') THEN  EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P3')||' from dual' into v_P3; END IF;
IF (XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P4')<>'#ERR') THEN  EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P4')||' from dual' into v_P4; END IF;
IF (XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P5')<>'#ERR') THEN  EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P5')||' from dual' into v_P5; END IF;
IF (XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P6')<>'#ERR') THEN  EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P6')||' from dual' into v_P6; END IF;
IF (XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P7')<>'#ERR') THEN  EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P7')||' from dual' into v_P7; END IF;
IF (XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P8')<>'#ERR') THEN  EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P8')||' from dual' into v_P8; END IF;
IF (XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P9')<>'#ERR') THEN  EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P9')||' from dual' into v_P9; END IF;
IF (XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P10')<>'#ERR') THEN  EXECUTE IMMEDIATE 'select '||XXCFI_UTILS_PKG.get_mapping('${P_cle_mapping}','P10')||' from dual' into v_P10; END IF;
 --
 v_conc_request_id := Fnd_Request.submit_request( application => 'XXFND'
                                                 ,program     => 'XXCFI_FND_SQL_MANUAL'
                                                 ,description => 'Script UNIX'
                                                 ,start_time  => NULL
                                                 ,sub_request => FALSE
                                                 ,argument1   => XXCFI_UTILS_PKG.get_requete_id(v_pgr)
                                                 ,argument2   => v_P1||'|'||v_P2||'|'||v_P3||'|'||v_P4||'|'||v_P5||'|'||v_P6||'|'||v_P7||'|'||v_P8||'|'||v_P9||'|'||v_P10
                                                 );
 COMMIT;
 --
  IF (v_conc_request_id = 0)
  THEN
     dbms_output.put_line ('FAILED##');
  ELSE
      v_dev_status := 'INITIAL';  
      v_dev_phase := 'INITIAL';              
      WHILE v_dev_phase != 'COMPLETE' LOOP
            v_execute_code := Fnd_Concurrent.WAIT_FOR_REQUEST ( v_conc_request_id   -- Current Concurrent Req ID
                                                              ,5                    -- No Of seconds to wait between checks
                                                              ,150                  -- Max no of seconds to wait
                                                              ,v_phase              -- Request Phase is OUT
                                                              ,v_status             -- Request Status is OUT
                                                              ,v_dev_phase          -- Dev Phase is OUT
                                                              ,v_dev_status         -- Dev Status is OUT
                                                              ,v_message);          -- Request completion message
            EXIT WHEN v_dev_phase = 'COMPLETE';
      END LOOP;
      dbms_output.put_line('SUCCESS'                        --[0] Resultat de la requete SQL
	                 ||'##'||v_conc_request_id              --[2] LRID du traitement simultan�
					 ||'##'||v_dev_status                   --[4]
					 ||'##'||v_dev_phase                    --[6]
					 ||'##'||v_pgr                          --[8]
					 ||'##'||v_uname                        --[10]
					 ||'##'||v_rname                        --[12]
					 ||'##'||v_fileout                      --[14] Chemin ou devra etre recopi� le fichier de sortie
					 ||'##'||v_description                  --[16] Description de l'en-tete de la table de mapping
					  --#V1.1 s.n. 
					 ||'##'||v_file_new_name                --[18] Nouveau nom du fichier de sortie
					  --#V1.1 e.n. 
					 ||'##');
	 --
  END IF;
  --
EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line('Error While Submitting Concurrent Request '||TO_CHAR(SQLCODE)||'-'||sqlerrm);
END;
/
EXIT;       
EOF`

#echo "============";
#echo ">"$resultat;

if expr match "$resultat" "SUCCESS"; then 
  echo "Requete SQL correctement executee."
  #
  IFS="##"
  set -A array $resultat
  echo "+---------------------------------------------------------------+"
  echo "         Informations provennant de l'application";
  echo "+---------------------------------------------------------------+"
  echo "${array[16]}"
  echo "+---------------------------------------------------------------+"
  echo "Statut du bloc PL/SQL             : "${array[0]}  #Resultat SQL SUCCESS/FAILURE
  echo "Concurrent program  -LRID         : "${array[2]}  #LRID
  echo "                    -Statut       : "${array[4]}  #Statut cp (NORMAL, WARNING, ERROR)
  echo '                    -Phase        : '${array[6]}  #phase cp (PENDING,COMPLETE....)
  echo 'Nom de la requete                 : '${array[8]}  #Nom de la requete
  echo 'Utilisateur de connexion          : '${array[10]} #Utilisateur de lancement
  echo 'Responsabilite                    : '${array[12]} #resp. de lancement
  #
  #Gestion fichier de sortie
  if [ ${array[14]} != "NA" ]; then
    v_device=${array[14]} #l'Emplacement destination du fichier de sortie ne contient pas de $ dans la table de mapping
    eval "v_device2=\$$v_device" #Le backslash permet de neutraliser le $ afin qu'il ne soit pas interpr�t� (sinon $$ = pid)
  else
    v_device2=${array[14]}
  fi
  echo 'Fichier de sortie a recopier sous : '${v_device2} #Emplacement destination du fichier de sortie
  echo 'Renommage du fichier de sortie    : '${array[18]} #Nouveau nom du fichier de sortie
  #
  echo "+---------------------------------------------------------------+"
  #
  #
  #
  echo " "
  echo "Traitement simultane en cours d'execution..."
  while [ ! -f $APPLCSF/out/o${array[2]}.out ./o${array[2]}.out ]; do sleep 1; done
  echo "Fichier de sortie: " $APPLCSF/out/o${array[2]}.out
  #
  #Copie + renommage du fichier dans le dossier pr�cis�
  #        Le fichier sera copi� et renomm� [Nom mapping ou nom requete]__[AAAAMMDDHHMISS]__[LRID].out
  #v_timestamp=${date '+%Y%m%d%H%M%S'}
  if [ "${array[14]}" != "NA" ]; then
	  echo "Le fichier de sortie doit etre copie dans : "${v_device2}
	  #Si le fichier doit etre renomm�:
	  if [ "${array[18]}" != "NA" ]; then v_file_name=${array[18]}"__"$(date '+%Y%m%d%H%M%S')"__"o${array[2]}.out
	  else
	   v_file_name=${array[8]}"__"$(date '+%Y%m%d%H%M%S')"__"o${array[2]}.out
	  fi
	  #
      cp  $APPLCSF/out/o${array[2]}.out ${v_device2}/${v_file_name}
      ret=$?
      #echo "code retour de la copie:"$ret
      if [ $ret -ne 0 ]; then
        echo "###ERREUR### Le fichier de sortie [$APPLCSF/out/o${array[2]}.out}] n'a pas pu etre recopie dans "${v_device2}
        exit 2
      else
        echo "Une copie du fichier de sortie se trouve sous " ${v_device2}/${v_file_name}
      fi
  else
      echo "Il n'est pas demande de copier le fichier de sortie."
  fi
  #
  while [ ! -f $APPLCSF/log/l${array[2]}.req ./l${array[2]}.req ]; do sleep 1; done
  echo "Journal du traitement : " $APPLCSF/log/l${array[2]}.req
  #
else 
  echo "##ERREUR## Une erreur a �t� d�tectee lors de l'execution de la requete SQL"  
  echo ">"$resultat;
  exit 2
fi
#
#Gestion du code retour
case ${array[4]} in
  'NORMAL')
    return_code=0
    ;;
  'WARNING')
    return_code=1 
    ;;
  'ERROR')
    return_code=2
    ;;
	*)
    return_code=3
    ;;
esac


echo "                                                         fin [$?]."
echo "+---------------------------------------------------------------+"
