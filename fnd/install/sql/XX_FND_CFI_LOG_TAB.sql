SET ECHO ON

/**
 * "@(#) XX_FND_CFI_LOG_TAB.sql : 12.2.9.01"
 * +---------------------------------------------------------------------------+
 * |                                                                           |
 * |                     Copyright (c) 2022 �tat de Gen�ve                     |
 * |                            All rights reserved                            |
 * |                                                                           |
 * +---------------------------------------------------------------------------+
 *
 *  Application      : CFI
 *  Livrable         :
 *  Auteur           : Judica�l MORIS
 *  Date de cr�ation : 17.10.2022
 *  Description      : Script de cr�ation de la table de log sp�cifique
 *
 * +------------+------------------+-----------+-------------------------------+
 * | Date       | Auteur           | Ver.      | Description du changement     |
 * +------------+------------------+-----------+-------------------------------+
 *   17.10.2022   Judica�l MORIS     12.2.9.01  Version Initiale
 */

-- En cas d'erreur, on repositionne la sortie avec le code erreur SQL
WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

DECLARE
  xt        XX_TABLE;
  ln_i      NUMBER;
BEGIN
  XX_FND_LOGGER.SET_LOG_LEVEL('INFO');
  XT := XX_TABLE('XX_FND_CFI_LOG', 'XXCFI');
  XT.SET_ID_COLUMN('LOG_SEQUENCE', 'NUMBER');
  XT.SET_COLUMN('LOG_TIME', 'DATE', NULL, NULL, 'SYSDATE');
  XT.SET_COLUMN('LOG_TEXT', 'CLOB');
  XT.SET_COLUMN('MODULE', 'VARCHAR2', 255);
  XT.SET_COLUMN('USER_ID', 'NUMBER');
  XT.SET_COLUMN('RESP_ID', 'NUMBER');
  XT.SET_COLUMN('SESSION_ID', 'NUMBER');
  XT.ALTER_DB;
  XT.REORDER_COLUMNS;

  XX_FND_LOGGER.INFO(XT.GET_NAME, 'Cr�ation des indexes', 'HEADER');

  XX_FND_LOGGER.INFO(XT.GET_NAME, '+ Cr�ation de l''index ' || XT.GET_NAME || '_U1');
  BEGIN
    SELECT  1
      INTO  ln_i
      FROM  ALL_INDEXES AI
     WHERE  AI.INDEX_NAME = XT.GET_NAME || '_U1'
       AND  AI.OWNER = XT.GET_OWNER;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      EXECUTE IMMEDIATE 
        'CREATE INDEX ' ||
        XT.GET_OWNER || '.' || XT.GET_NAME || '_U1 ON ' ||
        XT.GET_OWNER || '.' || XT.GET_NAME || ' (LOG_SEQUENCE)';
  END;

END;
/
SHOW ERROR

EXIT;
