set echo on

--# "@(#) XXCFI_RandomUUID_JAVA.sql : 1.0"
--##############################################################################
--#
--#                 Copyright (c) 2012 Etat de Gen�ve
--#                         All rights reserved
--#
--##############################################################################
--#
--# Application         : CFI
--# Livrable            :
--# Auteur              : Fabrice Houssier
--# Date de cr�ation    : 12.01.2018
--# Description         : Ce script cree la classe RandomUUID
--#
--#
--# Historique
--# --------------
--#
--# Date              Auteur                 Ver. Description du changement
--# ---------------- ----------------        ---- -----------------------------------
--# 12.01.2018       F. Houssier              1.0  DAME 7994 - Ajout fonction UUID     
--##############################################################################
--
--
-- En cas d'erreur, on repositionne la sortie avec le code erreur SQL

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

PROMPT ==========================================================================
PROMPT     Creation de la fonction java RandomUUID
PROMPT ==========================================================================

create or replace and compile
   java source named "RandomUUID"
   as
   public class RandomUUID
   {
      public static String create()
      {
              return java.util.UUID.randomUUID().toString();
      }
   }
/

PROMPT ==========================================================================
PROMPT     Fin OK
PROMPT ==========================================================================


EXIT;