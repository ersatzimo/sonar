set echo on

-- En cas d'erreur, on repositionne la sortie avec le code erreur SQL

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

-------------------------------------------------------
-- PACKAGE XXCFI_UTILS_PKG
--------------------------------------------------------
CREATE OR REPLACE PACKAGE apps.XXCFI_UTILS_PKG AS

--# "@(#) XXCFI_UTILS_PKG.sql : 12.2.9.05"
/**
 * +---------------------------------------------------------------------------+
 * |                                                                           |
 * |                    Copyright (c) 2023 �tat de Gen�ve                      |
 * |                           All rights reserved                             |
 * |                                                                           |
 * +---------------------------------------------------------------------------+
 *
 *  Application      : CFI
 *  Livrable         :
 *  Auteur           : Ludo MAZZEI
 *  Date de cr�ation : 06.07.2017
 *  Description      : Ce script cree le package XXCFI_UTILS_PKG, package
 *                     regroupant des fonctions et proc�dures pouvant �tre
 *                     utilis�es par les diff�rents composants 'CFI'
 *
 * +-----------+-------------------+-----------+-------------------------------+
 * | Date      | Auteur            | Version   | Description du changement     |
 * +-----------+-------------------+-----------+-------------------------------+
 *   24.05.2012  L. Mazzei                 1.0   Cr�ation
 *   01.12.2014  Gildas Bardon             1.1   DAME8232 Ajout de la fonction is_date
 *   12.04.2017  L. Mazzei                 1.2   DAME9762 Ajout de fonctions pour shell unix (ctrl-M)
 *   10.05.2017  L. Mazzei                 1.3   DAME9762 Gestion renommage fichier de sortie
 *   04.07.2017  L. Mazzei                 1.4   Ajout fonction get_org_id
 *   12.01.2018  F. Houssier               1.5   DAME 7994 - ajout fonction UUID
 *   12.11.2018  L. Mazzei                 1.6   Projet-2838:Ajout fonction get_value_description/get_po_vendor_num
 *   12.08.2021  G. Bardon                 1.7   Projet-10038:Ajout fonction get_group_id
 *   28.03.2022  PJ Giraud           12.2.9.01   Procedure/fonction pour check environnement OEBS
 *   17.10.2022  Judica�l MORIS      12.2.9.02   ALM-815 DI-D�mat : Aide en ligne sur notification GED LOT3
 *   20.03.2023  Guillaume MARTIN    12.2.9.03   ALM 901 - Prise en compte des contraintes minimales ADOP
 *   19.04.2022  PJ Giraud           12.2.9.04   Procedure/fonction pour check environnement OEBS
 *   27.04.2023  PJ Giraud           12.2.9.05   ALM917 mise � jour du fc|p_clean_dee_wferr
 */
--
--
--********************************************************************************************************
--********                     F o n c t i o n s     t e c h n i q u e s                         *********
--********************************************************************************************************
--
--
------------------------------------------------------------------------------------
-- NAME        : is_number
-- DESCRIPTION : Retourne TRUE si la chaine en param�tre est au format NUMBER sinon FALSE
-- PARAMETERS  : 1: Chaine de caract�re
-- RETURNS     : TRUE/FALSE
----------------------------------------------------------------------------------------
FUNCTION is_number(p_str IN VARCHAR2) RETURN BOOLEAN;
--
------------------------------------------------------------------------------------
-- NAME        : get_mapping
-- DESCRIPTION : Retourne la valeur associ� au code dans la table de mapping 'P_map_table'
-- PARAMETERS  : 1: Nom table de mapping
--               2: code
-- RETURNS     : Valeur correspondant au code
----------------------------------------------------------------------------------------
FUNCTION get_mapping(P_map_table IN VARCHAR2, P_code_in IN VARCHAR2) RETURN VARCHAR2;
--
------------------------------------------------------------------------------------
-- NAME        : get_mapping_description
-- DESCRIPTION : Retourne la description de la table de mapping 'P_map_table'
-- PARAMETERS  : 1: Nom table de mapping
-- RETURNS     : Description
----------------------------------------------------------------------------------------
FUNCTION get_mapping_description(P_map_table IN VARCHAR2) RETURN VARCHAR2;
--
--#XX V1.6.s.n.
------------------------------------------------------------------------------------
-- NAME        : get_value_description
-- DESCRIPTION : Retourne la description d'une valeur associ� � un jeu de valeurs
-- PARAMETERS  : 1. Nom du jeu de valeurs
--               2. Valeur issue du jeu de valeurs
-- RETURNS     : Description
----------------------------------------------------------------------------------------
FUNCTION get_value_description(P_value_set IN VARCHAR2, P_value IN VARCHAR2) RETURN VARCHAR2;
--#XX V1.6.e.n.
--
------------------------------------------------------------------------------------
-- NAME        : is_date
-- DESCRIPTION : Retourne TRUE si la chaine en param�tre est au format DATE sinon FALSE
-- PARAMETERS  : 1: Chaine de caract�re
--               2: Format
-- RETURNS     : TRUE/FALSE
----------------------------------------------------------------------------------------
FUNCTION is_date(p_field IN VARCHAR2, p_format IN VARCHAR2) RETURN VARCHAR2; -- #XX V1.1.new
--
------------------------------------------------------------------------------------
-- NAME        : get_cte
-- DESCRIPTION : Retourne la constante plac�e dans la chaine de caract�re
-- PARAMETERS  : 1: chaine de caract�re
-- RETURNS     : 1: chaine de caract�re
----------------------------------------------------------------------------------------
FUNCTION get_cte(P_str IN VARCHAR2) RETURN VARCHAR2;
--
------------------------------------------------------------------------------------
-- NAME        : get_requete_id
-- DESCRIPTION : A partir du nom d'une requete (au sens du requeteur), cette
--               fonction retourne son id.
-- PARAMETERS  : 1: Nom de la requete
-- RETURNS     : Identifiant de la requete
----------------------------------------------------------------------------------------
FUNCTION get_requete_id(p_req_name IN VARCHAR2) RETURN NUMBER;
--
--#XX V1.5.sn
------------------------------------------------------------------------------------
-- NAME        : RandomUUID
-- DESCRIPTION : Retourne un identifiant UUID
-- PARAMETERS  :
-- RETURNS     : Cha�ne de caract�res
----------------------------------------------------------------------------------------
FUNCTION RandomUUID RETURN VARCHAR2;
--
------------------------------------------------------------------------------------
-- NAME        : LOGGER
-- DESCRIPTION : Alimente la table de log
-- PARAMETERS  :
-- RETURNS     :
----------------------------------------------------------------------------------------
PROCEDURE LOGGER (  MODULE          IN  VARCHAR2,
                    TEXTE           IN  VARCHAR2,
                    TIME            IN  DATE DEFAULT SYSDATE,
                    USER_ID         IN  NUMBER DEFAULT FND_GLOBAL.USER_ID,
                    RESP_ID         IN  NUMBER DEFAULT FND_GLOBAL.RESP_ID,
                    SESSION_ID      IN  NUMBER DEFAULT FND_GLOBAL.SESSION_ID
                 );
--
--
--********************************************************************************************************
--********           F o n c t i o n s     l i � e s     a u x     p � r i o d e s               *********
--********************************************************************************************************
--
------------------------------------------------------------------------------------
-- NAME        : get_first_open_gl_period
-- DESCRIPTION : Retourne la 1ere p�riode GL ouverte
-- PARAMETERS  :
-- RETURNS     : MON-YYYY (1ere p�riode GL Ouverte)
----------------------------------------------------------------------------------------
FUNCTION get_first_open_gl_period RETURN VARCHAR2;
--
------------------------------------------------------------------------------------
-- NAME        : get_last_day_previous_month
-- DESCRIPTION : Retourne le dernier jour du mois pr�c�dent
-- PARAMETERS  : format attendu
-- RETURNS     : Retourne le dernier jour du mois pr�c�dent
----------------------------------------------------------------------------------------
FUNCTION get_last_day_previous_month(P_format IN VARCHAR2) RETURN VARCHAR2;
--
--********************************************************************************************************
--********              F o n c t i o n s     l i � e s     a u x     d a t e s                  *********
--********************************************************************************************************
--
------------------------------------------------------------------------------------
-- NAME        : get_sysdate
-- DESCRIPTION : Retourne la date du jour [avec eventuellement +- njours] au format pass� en param�tre
-- PARAMETERS  : 1: chaine de caract�re
-- RETURNS     : Date
----------------------------------------------------------------------------------------
FUNCTION get_sysdate(P_format IN VARCHAR2 DEFAULT 'DD.MM.RRRR', P_nbr_j IN NUMBER DEFAULT 0) RETURN VARCHAR2;
--
--
--********************************************************************************************************
--********      F o n c t i o n s     l i � e s     a u x     o r g a n i s a t i o n s          *********
--********************************************************************************************************
--
--
------------------------------------------------------------------------------------
-- NAME        : get_org_id
-- DESCRIPTION : l'identifiant de l'organisation pass�e en param�tre
-- PARAMETERS  : 1: Nom de l'organization
-- RETURNS     : Organization_id
----------------------------------------------------------------------------------------
FUNCTION get_org_id(P_org_name IN VARCHAR2) RETURN VARCHAR2;
--
--
--********************************************************************************************************
--********         F o n c t i o n s     l i � e s     a u x   e m p l o y e s                   *********
--********************************************************************************************************
--
--
--
------------------------------------------------------------------------------------
-- NAME        : get_employee_full_name
-- DESCRIPTION : retourne le nom de la personne associ�e au username pass� en param�tre
--               fonction retourne son id.
-- PARAMETERS  : 1: username
-- RETURNS     : nom de la personne
----------------------------------------------------------------------------------------
FUNCTION get_employee_full_name(p_username IN VARCHAR2) RETURN VARCHAR2;
--
--
--
--
--********************************************************************************************************
--********         F o n c t i o n s     l i � e s     a u    m o d u l e   A R                  *********
--********************************************************************************************************
--
--
--
--
--
------------------------------------------------------------------------------------
-- NAME        : get_ra_batch_source_id
-- DESCRIPTION : l'identifiant du lot pass� en param�tre
-- PARAMETERS  : 1: Nom du lot
-- RETURNS     : batch_source_id
----------------------------------------------------------------------------------------
FUNCTION get_ra_batch_source_id(P_batch_name IN VARCHAR2) RETURN VARCHAR2;


--
--
--********************************************************************************************************
--********         F o n c t i o n s     l i � e s     a u    m o d u l e   P O                  *********
--********************************************************************************************************
--
--
--
--
--
--#XX V1.6.s.n.
------------------------------------------------------------------------------------
-- NAME        : get_po_vendor_num
-- DESCRIPTION : Retourne le num�ro fournisseur � partir de son id
-- PARAMETERS  : 1: ide du fournisseur
-- RETURNS     : Num�ro (ap_suppliers.segment1)
----------------------------------------------------------------------------------------
FUNCTION get_po_vendor_num(P_vendor_id IN VARCHAR2) RETURN VARCHAR2;
--#XX V1.6.e.n.

--
--
--
--
--********************************************************************************************************
--********         F o n c t i o n s     l i � e s     a u    m o d u l e   G L                  *********
--********************************************************************************************************
--
--
--
--
--
--#XX V1.7.sn
------------------------------------------------------------------------------------
-- NAME        : get_group_id
-- DESCRIPTION : renvoi le group_id de rang le plus �lev� pour une origine donn�e
-- PARAMETERS  : 1: Origine de la pipce
-- RETURNS     : group_id
----------------------------------------------------------------------------------------
FUNCTION get_group_id(P_origine IN VARCHAR2) RETURN NUMBER;
--#XX V1.7.en

--********************************************************************************************************
--********                     C h e c k s    e n v i r o n n e m e n t     O E B S              *********
--********************************************************************************************************
--
------------------------------------------------------------------------------------
-- NAME        : clean_dee_wferr
-- DESCRIPTION : Nettoyage des Notif de WF en erreur en provenance d'un DEFAULT_EVENT_ERROR
-- script bde_end_date_wf_ntfs_event_errors.sql du WF Analyzer report (1369938.1)
-- End dates all open wferr Error Event Notifications that have a root activity of DEFAULT_EVENT_ERROR.
-- PARAMETERS  :
-- + p_item_type            = va traiter les wf_item dont item_type = p_item_type (param�tre obligatoire)
-- + p_item_key             = va traiter les wf_item dont item_key = p_item_key (param�tre facultatif)
-- + p_root_activity        = va traiter les wf_item dont root_activity like p_root_activity (param�tre facultatif)
-- + p_parent_item_type     = va traiter les wf_item dont parent_item_type = p_parent_item_type (param�tre facultatif)
-- + p_age                  = va traiter les wf_item d�marr�s avant J - p_age (param�tre facultatif)
-- + p_commit [Y|y|O|o]     = commit si fin 0 ou 1, sinon ROLLBACK
-- + p_log_level            = log level utilis� par XX_FND_LOGGER
-- RETURNS     : [0=OK, 1=warning, 2=error]
----------------------------------------------------------------------------------------
FUNCTION fc_clean_dee_wferr(
     p_item_type                    IN  wf_items.item_type%TYPE                 := 'WFERROR'
    ,p_item_key                     IN  wf_items.item_key%TYPE                  := NULL
    ,p_root_activity                IN  wf_items.root_activity%TYPE             := 'DEFAULT_EVENT%'
    ,p_parent_item_type             IN  wf_items.parent_item_type%TYPE          := NULL
    ,p_age                          IN  NUMBER                                  := NULL
    ,p_commit                       IN  CHAR                                    := 'Y'
    ,p_log_level                    IN  XX_FND_LOGGER.gn_session_log_level%TYPE := 3
    )
    RETURN NUMBER;
PROCEDURE p_clean_dee_wferr(
     errbuf                         OUT VARCHAR2
    ,retcode                        OUT VARCHAR2
    ,p_item_type                    IN  wf_items.item_type%TYPE                 := 'WFERROR'
    ,p_item_key                     IN  wf_items.item_key%TYPE                  := NULL
    ,p_root_activity                IN  wf_items.root_activity%TYPE             := 'DEFAULT_EVENT%'
    ,p_parent_item_type             IN  wf_items.parent_item_type%TYPE          := NULL
    ,p_age                          IN  NUMBER                                  := NULL
    ,p_commit                       IN  CHAR                                    := 'Y'
    ,p_log_level                    IN  XX_FND_LOGGER.gn_session_log_level%TYPE := 3
    );
------------------------------------------------------------------------------------
-- NAME        : fc_hold_requests
-- DESCRIPTION : Changement statut HOLD des concurrent requests
-- PARAMETERS  :
-- + p_hold_curr [Y|N]      = valeur hold_flag � traiter
-- + p_hold_new [Y|N]       = valeur hold_flag qui va �tre mis sur les requests trait�es
-- + p_request_id           = pour ne traiter qu'une request
-- + p_phase_code           = le code phase que l'on traite (NULL=on ne tient pas compte du code phase)
-- + p_requested_by_curr    = user des requests ayant fait une mise � jour (cela nous sert pour identifier les requests � d�bloquer)
-- + p_requested_by_new     = user des requests qui va �tre mis sur les requests trait�es
-- + p_commit [Y|y|O|o]     = commit si fin 0 ou 1, sinon ROLLBACK
-- + p_log_level            = log level utilis� par XX_FND_LOGGER
-- RETURNS     : [0=OK, 1=warning, 2=error]
----------------------------------------------------------------------------------------
FUNCTION fc_hold_requests(
     p_hold_curr                    IN  CHAR
    ,p_hold_new                     IN  CHAR
    ,p_updated_by_curr              IN  fnd_user.user_id%TYPE                   := NULL
    ,p_updated_by_new               IN  fnd_user.user_id%TYPE                   := NULL
    ,p_request_id                   IN  fnd_concurrent_requests.request_id%TYPE := NULL
    ,p_phase_code                   IN  fnd_concurrent_requests.phase_code%TYPE := 'P'
    ,p_requested_by                 IN  fnd_user.user_id%TYPE                   := NULL
    ,p_commit                       IN  CHAR                                    := 'Y'
    ,p_log_level                    IN  XX_FND_LOGGER.gn_session_log_level%TYPE := 3
    )
RETURN NUMBER;
PROCEDURE p_hold_requests(
     errbuf                         OUT VARCHAR2
    ,retcode                        OUT VARCHAR2
    ,p_hold_curr                    IN  CHAR
    ,p_hold_new                     IN  CHAR
    ,p_updated_by_curr              IN  fnd_user.user_id%TYPE                   := NULL
    ,p_updated_by_new               IN  fnd_user.user_id%TYPE                   := NULL
    ,p_request_id                   IN  fnd_concurrent_requests.request_id%TYPE := NULL
    ,p_phase_code                   IN  fnd_concurrent_requests.phase_code%TYPE := 'P'
    ,p_requested_by                 IN  fnd_user.user_id%TYPE                   := NULL
    ,p_commit                       IN  CHAR                                    := 'Y'
    ,p_log_level                    IN  XX_FND_LOGGER.gn_session_log_level%TYPE := 3
    );
END XXCFI_UTILS_PKG;
/
SHOW ERROR

--







---------------------------------------------------------------------
-- PACKAGE BODY XXCFI_UTILS_PKG
---------------------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY APPS.XXCFI_UTILS_PKG AS

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- PRIVATE CONSTANTS AND VARIABLES
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- constants
    cv_package_name             CONSTANT    VARCHAR2(21):= 'XXCFI_UTILS_PKG';




--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- PUBLIC FUNCTIONS AND PROCEDURES
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--
--********************************************************************************************************
--********                     F o n c t i o n s     t e c h n i q u e s                         *********
--********************************************************************************************************
--
--
FUNCTION is_number( p_str IN VARCHAR2 )
  RETURN BOOLEAN
IS
  l_num NUMBER;
BEGIN
  l_num := to_number( NVL(p_str,0) );
  RETURN TRUE;
EXCEPTION
  WHEN others THEN
    RETURN FALSE;
END is_number;

---------------------------------------------------------------------------------------------

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- Retourne le code P_code_in issu de la table de mapping P_map_table
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
FUNCTION get_mapping(P_map_table IN VARCHAR2, P_code_in IN VARCHAR2)
 RETURN VARCHAR2
 IS
 L_code_out XXCFI_FND_MAPPING_LINES.data_out%TYPE;
 BEGIN
   SELECT L.data_out
   INTO L_code_out
   FROM XXCFI_FND_MAPPING_HEADERS H, XXCFI_FND_MAPPING_LINES L
   WHERE l.mapping_name = H.mapping_name
   AND h.mapping_name = P_map_table
   and data_in = P_code_in;
   --
   RETURN(L_code_out);
   --
EXCEPTION
  WHEN OTHERS THEN
    RETURN '#ERR';
END get_mapping;
--
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- Retourne la description associ�e � une table de mapping P_map_table
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
FUNCTION get_mapping_description(P_map_table IN VARCHAR2)
 RETURN VARCHAR2
 IS
 v_description XXCFI_FND_MAPPING_LINES.data_out%TYPE;
 BEGIN
   SELECT H.description
   INTO v_description
   FROM XXCFI_FND_MAPPING_HEADERS H
   WHERE  h.mapping_name = P_map_table;
   --
   RETURN(v_description);
   --
EXCEPTION
  WHEN OTHERS THEN
    RETURN '#ERR';
END get_mapping_description;



--#XX V1.6.s.n.
------------------------------------------------------------------------------------
-- NAME        : get_value_description
-- DESCRIPTION : Retourne la description d'une valeur associ�s � un jeu de valeurs
-- PARAMETERS  : 1. Nom du jeu de valeurs
--               2. Valeur issue du jeu de valeurs
-- RETURNS     : Description
----------------------------------------------------------------------------------------
FUNCTION get_value_description(P_value_set IN VARCHAR2, P_value IN VARCHAR2)
 RETURN VARCHAR2
 IS
 v_description FND_FLEX_VALUES_TL.description%TYPE;
 BEGIN
   SELECT vtl.description
   INTO v_description
   FROM fnd_flex_value_sets s
      , fnd_flex_values v
     , fnd_flex_values_tl vtl
   WHERE s.flex_value_set_name = P_value_set
   AND v.FLEX_VALUE_SET_ID = s.FLEX_VALUE_SET_ID
   AND vtl.flex_value_id = v.flex_value_id
   AND vtl.language='F'
   AND flex_value = P_value;
   --
   RETURN(v_description);
   --
EXCEPTION
  WHEN OTHERS THEN
    RETURN '#ERR';
END get_value_description;
--#XX V1.6.e.n.

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- FUNCTION is_date
--
-- V�rifie si un champ est bien de type DATE
-- @param p_field : Valeur � tester
-- @param p_format : Format de DATE � respecter
-- @return : "Y" si la valeur est une date, "N" sinon
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
FUNCTION is_date(p_field IN VARCHAR2, p_format IN VARCHAR2) RETURN VARCHAR2
IS
	l_dummy DATE;
BEGIN
    IF p_field IS NOT NULL AND p_format IS NOT NULL THEN
		SELECT to_date(p_field, p_format)
		  INTO l_dummy
		  FROM dual;
		RETURN 'Y';
	ELSE
		RETURN 'N';
	END IF;
EXCEPTION WHEN OTHERS THEN
	RETURN 'N';
END is_date;
--
------------------------------------------------------------------------------------
-- NAME        : get_cte
-- DESCRIPTION : Retourne la constante plac�e dans la chaine de caract�re
-- PARAMETERS  : 1: chaine de caract�re
-- RETURNS     : 1: chaine de caract�re
----------------------------------------------------------------------------------------
FUNCTION get_cte(P_str IN VARCHAR2) RETURN VARCHAR2
IS
BEGIN
RETURN(P_str);
EXCEPTION
	WHEN OTHERS THEN RETURN(P_str);
END get_cte;
--
------------------------------------------------------------------------------------
-- NAME        : get_requete_id
-- DESCRIPTION : A partir du nom d'un requete (au sens du requeteur), cette
--               fonction retourne son id.
-- PARAMETERS  : 1: Nom de la requete
-- RETURNS     : Identifiant de la requete
----------------------------------------------------------------------------------------
FUNCTION get_requete_id(p_req_name IN VARCHAR2) RETURN NUMBER
IS
v_req_id NUMBER;
BEGIN
  SELECT  FFS.ID_FLEX_NUM
  INTO v_req_id
  FROM FND_ID_FLEX_STRUCTURES_TL FFS
      ,XXCFI_FND_SQL_REPORTS  FSR
  WHERE FFS.ID_FLEX_CODE = 'XRP#'
        AND FFS.APPLICATION_ID = 20011
        AND FFS.LANGUAGE = 'F'
        AND FFS.ID_FLEX_STRUCTURE_NAME = FSR. REPORT_CODE
        AND FSR.ENABLED_FLAG = 'Y'
        AND FSR.RUN_MODE IN ('M', 'B')
        AND FSR.APPLICATION_ID = FND_GLOBAL.RESP_APPL_ID
		AND (FSR.ENABLE_SECURITY = 'N' OR (FSR.ENABLE_SECURITY = 'Y'
		AND ( SELECT COUNT(1)
			  FROM FND_FLEX_VALUE_SETS FFVS,
                   FND_FLEX_VALUES FFV
              WHERE  FFV.FLEX_VALUE_SET_ID = FFVS.FLEX_VALUE_SET_ID
                      AND FFV.ENABLED_FLAG = 'Y'
					  AND FFVS.FLEX_VALUE_SET_ID = FND_PROFILE.value('XXCFI_FND_QUERY_LIST')
					  AND FSR.REPORT_CODE = FFV.FLEX_VALUE) > 0))
        AND ID_FLEX_STRUCTURE_NAME = p_req_name;
--
RETURN(v_req_id);
END get_requete_id;
--
--#XX V1.5.sn
------------------------------------------------------------------------------------
-- NAME        : RandomUUID
-- DESCRIPTION : Retourne un identifiant UUID
-- PARAMETERS  :
-- RETURNS     : Cha�ne de caract�res
----------------------------------------------------------------------------------------
FUNCTION RandomUUID
   RETURN VARCHAR2
   AS LANGUAGE JAVA
   NAME 'RandomUUID.create() return java.lang.String';
--#XX V1.5.en
--
------------------------------------------------------------------------------------
-- NAME        : LOGGER
-- DESCRIPTION : Alimente la table de log
-- PARAMETERS  :
-- RETURNS     :
----------------------------------------------------------------------------------------
PROCEDURE LOGGER (  MODULE          IN  VARCHAR2,
                    TEXTE           IN  VARCHAR2,
                    TIME            IN  DATE DEFAULT SYSDATE,
                    USER_ID         IN  NUMBER DEFAULT FND_GLOBAL.USER_ID,
                    RESP_ID         IN  NUMBER DEFAULT FND_GLOBAL.RESP_ID,
                    SESSION_ID      IN  NUMBER DEFAULT FND_GLOBAL.SESSION_ID
                 ) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    INSERT INTO XX_FND_CFI_LOG (LOG_TIME,
                                LOG_TEXT,
                                MODULE,
                                USER_ID,
                                RESP_ID,
                                SESSION_ID)
    VALUES (TIME,
            TEXTE,
            MODULE,
            USER_ID,
            RESP_ID,
            SESSION_ID);
    COMMIT;
END LOGGER;
--
--
--
--********************************************************************************************************
--********           F o n c t i o n s     l i � e s     a u x     p � r i o d e s               *********
--********************************************************************************************************
--
--
------------------------------------------------------------------------------------
-- NAME        : get_first_open_gl_period
-- DESCRIPTION : Retourne la 1ere p�riode GL ouverte
-- PARAMETERS  :
-- RETURNS     : MON-YYYY (1ere p�riode GL Ouverte)
----------------------------------------------------------------------------------------
FUNCTION get_first_open_gl_period RETURN VARCHAR2
IS
v_gl_open_period GL_PERIOD_STATUSES.period_name%TYPE;
BEGIN
  SELECT  GLP.period_name
  INTO v_gl_open_period
  FROM GL_PERIOD_STATUSES GLP
      ,FND_APPLICATION APP
  WHERE APP.APPLICATION_SHORT_NAME = 'SQLGL'
  AND GLP.application_id = APP.application_id
  AND GLP.set_of_books_id= fnd_profile.value('GL_SET_OF_BKS_ID')
  AND GLP.adjustment_period_flag = 'N'
  AND GLP.closing_status = 'O'
  AND GLP.effective_period_num = (SELECT  MIN(effective_period_num)
                                         FROM GL_PERIOD_STATUSES GLP2
                                         WHERE GLP2.application_id = GLP.application_id
                                         AND GLP2.set_of_books_id = GLP.set_of_books_id
                                         AND GLP2.adjustment_period_flag = GLP.adjustment_period_flag
                                         AND GLP2.closing_status = GLP.closing_status
                                         );
  --
  return(v_gl_open_period);
EXCEPTION
	WHEN OTHERS THEN v_gl_open_period:='#ERR#';
                   return(v_gl_open_period);
END get_first_open_gl_period;
--
------------------------------------------------------------------------------------
-- NAME        : get_last_day_previous_month
-- DESCRIPTION : Retourne le dernier jour du mois pr�c�dent
-- PARAMETERS  : format attendu
-- RETURNS     : Retourne le dernier jour du mois pr�c�dent
----------------------------------------------------------------------------------------
FUNCTION get_last_day_previous_month(P_format IN VARCHAR2) RETURN VARCHAR2
IS
v_date VARCHAR2(30);
BEGIN
 SELECT TO_CHAR(LAST_DAY(ADD_MONTHS(sysdate,-1)) , P_format)
 INTO v_date
 FROM dual;
 --
 RETURN(v_date);
EXCEPTION
 WHEN NO_DATA_FOUND THEN RETURN('#NDF#');
 WHEN TOO_MANY_ROWS THEN RETURN('#TMR#');
 WHEN OTHERS THEN RETURN('#ERR#');
END get_last_day_previous_month;
--
------------------------------------------------------------------------------------
-- NAME        : get_sysdate
-- DESCRIPTION : Retourne la date du jour [avec eventuellement +- njours] au format pass� en param�tre
-- PARAMETERS  : 1: chaine de caract�re
-- RETURNS     : Date
----------------------------------------------------------------------------------------
FUNCTION get_sysdate(P_format IN VARCHAR2, P_nbr_j IN NUMBER DEFAULT 0) RETURN VARCHAR2
IS
v_date VARCHAR2(20);
BEGIN
 SELECT TO_CHAR(SYSDATE+P_nbr_j,P_format)
 INTO v_date
 FROM DUAL;
 --
 RETURN(v_date);
END get_sysdate;
--
--
--********************************************************************************************************
--********      F o n c t i o n s     l i � e s     a u x     o r g a n i s a t i o n s          *********
--********************************************************************************************************
--
--
--
------------------------------------------------------------------------------------
-- NAME        : get_org_id
-- DESCRIPTION : l'identifiant de l'organisation pass�e en param�tre
-- PARAMETERS  : 1: Nom de l'organization
-- RETURNS     : Organization_id
----------------------------------------------------------------------------------------
FUNCTION get_org_id(P_org_name IN VARCHAR2) RETURN VARCHAR2
IS
v_org_id VARCHAR2(10);
BEGIN
 SELECT organization_id
 INTO v_org_id
 FROM hr_all_organization_units
 WHERE name = P_org_name;
 --
 RETURN(v_org_id);
EXCEPTION
 WHEN NO_DATA_FOUND THEN RETURN('#NDF#');
 WHEN TOO_MANY_ROWS THEN RETURN('#TMR#');
 WHEN OTHERS THEN RETURN('#ERR#');
END get_org_id;
--
--
--********************************************************************************************************
--********         F o n c t i o n s     l i � e s     a u x   e m p l o y e s                   *********
--********************************************************************************************************
--
--
--
------------------------------------------------------------------------------------
-- NAME        : get_employee_full_name
-- DESCRIPTION : retourne le nom de la personne associ� au username pass� en param�tre
--               fonction retourne son id.
-- PARAMETERS  : 1: username
-- RETURNS     : nom de la personne
----------------------------------------------------------------------------------------
FUNCTION get_employee_full_name(P_username IN VARCHAR2) RETURN VARCHAR2
IS
v_employee_full_name PER_ALL_PEOPLE_F.FULL_NAME%TYPE;
BEGIN
  SELECT NVL(decode(PEO.Last_name, Null, Null, PEO.Last_name||', ')||PEO.first_name, U.user_name)
  INTO v_employee_full_name
  FROM PER_ALL_PEOPLE_F PEO
     , FND_USER U
  WHERE  U.user_name = P_username
  AND PEO.person_id = U.employee_id;
  --
  RETURN(v_employee_full_name);
EXCEPTION
	WHEN OTHERS THEN v_employee_full_name:='#ERR#';
                   RETURN(v_employee_full_name);
--
END get_employee_full_name;
--
--
--
--********************************************************************************************************
--********         F o n c t i o n s     l i � e s     a u    m o d u l e   A R                  *********
--********************************************************************************************************
--
--
--
------------------------------------------------------------------------------------
-- NAME        : get_ra_batch_source_id
-- DESCRIPTION : l'identifiant du lot pass� en param�tre
-- PARAMETERS  : 1: Nom du lot
-- RETURNS     : batch_source_id
----------------------------------------------------------------------------------------
FUNCTION get_ra_batch_source_id(P_batch_name IN VARCHAR2) RETURN VARCHAR2
IS
v_batch_source_id VARCHAR2(10);
BEGIN
 SELECT batch_source_id
 INTO v_batch_source_id
 FROM ra_batch_sources_all
 WHERE name = P_batch_name;
 --
 RETURN(v_batch_source_id);
EXCEPTION
 WHEN NO_DATA_FOUND THEN RETURN(-1);
 WHEN TOO_MANY_ROWS THEN RETURN('#TMR#');
 WHEN OTHERS THEN RETURN('#ERR#');
END get_ra_batch_source_id;
--
--
--********************************************************************************************************
--********         F o n c t i o n s     l i � e s     a u    m o d u l e   P O                  *********
--********************************************************************************************************
--
--
--
--
--
--#XX V1.6.s.n.
------------------------------------------------------------------------------------
-- NAME        : get_vendor_num
-- DESCRIPTION : Retourne le num�ro fournisseur � partir de son id
-- PARAMETERS  : 1: id du fournisseur
-- RETURNS     : Num�ro (ap_suppliers.segment1)
----------------------------------------------------------------------------------------
FUNCTION get_po_vendor_num(P_vendor_id IN VARCHAR2) RETURN VARCHAR2
IS
v_vendor_num ap_suppliers.segment1%TYPE;
BEGIN
 SELECT segment1
 INTO v_vendor_num
 FROM ap_suppliers
 WHERE vendor_id = P_vendor_id;
 --
 RETURN(v_vendor_num);
EXCEPTION
 WHEN NO_DATA_FOUND THEN RETURN(-1);
 WHEN TOO_MANY_ROWS THEN RETURN('#TMR#');
 WHEN OTHERS THEN RETURN('#ERR#');
END get_po_vendor_num;
--#XX V1.6.e.n.
--
--
--
--********************************************************************************************************
--********         F o n c t i o n s     l i � e s     a u    m o d u l e   G L                  *********
--********************************************************************************************************
--
--
--
--
--
--#XX V1.7.sn
------------------------------------------------------------------------------------
-- NAME        : get_group_id
-- DESCRIPTION : renvoi le group_id de rang le plus �lev� pour une origine donn�e
-- PARAMETERS  : 1: Origine de la pipce
-- RETURNS     : group_id
----------------------------------------------------------------------------------------
FUNCTION get_group_id(P_origine IN VARCHAR2) RETURN NUMBER IS
v_group_id gl_interface.group_id%TYPE := NULL;
BEGIN
 SELECT MAX(group_id)
 INTO  v_group_id
 FROM  gl_interface
 WHERE user_je_source_name = P_origine
 AND   status = 'NEW'
 AND   group_id IS NOT NULL ;
 --
 RETURN(v_group_id);
EXCEPTION
 WHEN NO_DATA_FOUND THEN RETURN(NULL);
 WHEN OTHERS THEN RETURN(NULL);
END get_group_id;
--#XX V1.7.en
--

-- --#XX V12.2.9.01.s.n.
--********************************************************************************************************
--********                     C h e c k s    e n v i r o n n e m e n t     O E B S              *********
--********************************************************************************************************
--
------------------------------------------------------------------------------------
-- NAME        : clean_dee_wferr
-- DESCRIPTION : Nettoyage des Notif de WF en erreur en provenance d'un DEFAULT_EVENT_ERROR
-- script bde_end_date_wf_ntfs_event_errors.sql du WF Analyzer report (1369938.1)
-- End dates all open WFERROR Error Event Notifications that have a root activity of DEFAULT_EVENT_ERROR.
-- version du script utilis�e : $Header: bde_wf_end_date_ntf2.sql 1.04 2018/01/10 16:47:23 bburbage $
----------------------------------------------------------------------------------------
-- Version specifique permettant de traiter d'autres notifications que celle du script d'Oracle
-- attention donc de bien determiner les options de lancement de cette procedure
----------------------------------------------------------------------------------------
FUNCTION fc_clean_dee_wferr(
     p_item_type                    IN  wf_items.item_type%TYPE                 := 'WFERROR'
    ,p_item_key                     IN  wf_items.item_key%TYPE                  := NULL
    ,p_root_activity                IN  wf_items.root_activity%TYPE             := 'DEFAULT_EVENT%'
    ,p_parent_item_type             IN  wf_items.parent_item_type%TYPE          := NULL
    ,p_age                          IN  NUMBER                                  := NULL
    ,p_commit                       IN  CHAR                                    := 'Y'
    ,p_log_level                    IN  XX_FND_LOGGER.gn_session_log_level%TYPE := 3
    )
    RETURN NUMBER
    IS
    PRAGMA AUTONOMOUS_TRANSACTION;
cv_proc_name            CONSTANT    VARCHAR2(30)                     := 'fc_clean_dee_wferr';
cv_log_context          CONSTANT    VARCHAR2(2000)                   := lower(cv_package_name) || '.' || lower(cv_proc_name);
i_total number;
cursor c_err_items (
                     p_item_type                    IN  wf_items.item_type%TYPE                                       -- je ne defaulte pas car le param�tre est mandatory
                    ,p_item_key                     IN  wf_items.item_key%TYPE
                    ,p_root_activity                IN  wf_items.root_activity%TYPE             := 'DEFAULT_EVENT%'
                    ,p_parent_item_type             IN  wf_items.parent_item_type%TYPE          := NULL
                    ,p_age                          IN NUMBER
                    )
is
     select item_type, item_key
      from wf_items
      where item_type = p_item_type -- parametre mandatory
      and item_key = COALESCE(p_item_key, item_key)
      and root_activity like COALESCE(p_root_activity, root_activity)
      and NVL(parent_item_type,'null') = COALESCE(NVL(p_parent_item_type,'null'), NVL(parent_item_type,'null'))
      and begin_date < sysdate - COALESCE(p_age, 0)
      and end_date is null;
BEGIN -- fc_clean_dee_wferr
    xx_fnd_logger.set_log_level(p_log_level);
    xx_fnd_logger.info(cv_log_context, 'start');
    xx_fnd_logger.info(cv_log_context, 'param�tre         p_item_type:'||p_item_type);
    xx_fnd_logger.info(cv_log_context, 'param�tre          p_item_key:'||p_item_key);
    xx_fnd_logger.info(cv_log_context, 'param�tre     p_root_activity:'||p_root_activity);
    xx_fnd_logger.info(cv_log_context, 'param�tre  p_parent_item_type:'||p_parent_item_type);
    xx_fnd_logger.info(cv_log_context, 'param�tre               p_age:'||p_age);
    xx_fnd_logger.info(cv_log_context, 'param�tre            p_commit:'||p_commit);
    xx_fnd_logger.debug(cv_log_context, 'parametre         p_log_level:'||p_log_level);
    --
	i_total := 0;

    for e1 in c_err_items (p_item_type => p_item_type, p_item_key => p_item_key, p_root_activity => p_root_activity, p_parent_item_type => p_parent_item_type, p_age => p_age)
    loop
        xx_fnd_logger.debug(cv_log_context, 'item_type:'||e1.item_type);
        xx_fnd_logger.debug(cv_log_context, 'item_key:'||e1.item_key);
        xx_fnd_logger.debug(cv_log_context, 'wf_engine.AbortProcess(e1.item_type, e1.item_key, cascade=>FALSE)');
        wf_engine.AbortProcess(e1.item_type, e1.item_key, cascade=>FALSE);
        i_total := i_total+1;
    end loop;
    xx_fnd_logger.info(cv_log_context, 'Successfully aborted '|| i_total||' items using wf_engine.AbortProcess().');
    --
    IF UPPER(substr(p_commit,1,1)) IN ('Y','O') THEN COMMIT; xx_fnd_logger.info(cv_log_context, 'COMMIT eff�cut�'); ELSE ROLLBACK; xx_fnd_logger.info(cv_log_context, 'ROLLBACK eff�cut�'); END IF;
    xx_fnd_logger.info(cv_log_context, 'fin normale');
    RETURN 0;
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK; xx_fnd_logger.info(cv_log_context, 'ROLLBACK eff�cut�');
    RAISE; -- exception non trait�e (la procedure appelant plantera si aucun traitement de cette exception)
END fc_clean_dee_wferr;
PROCEDURE p_clean_dee_wferr(
     errbuf                         OUT VARCHAR2
    ,retcode                        OUT VARCHAR2
    ,p_item_type                    IN  wf_items.item_type%TYPE                 := 'WFERROR'
    ,p_item_key                     IN  wf_items.item_key%TYPE                  := NULL
    ,p_root_activity                IN  wf_items.root_activity%TYPE             := 'DEFAULT_EVENT%'
    ,p_parent_item_type             IN  wf_items.parent_item_type%TYPE          := NULL
    ,p_age                          IN  NUMBER                                  := NULL
    ,p_commit                       IN  CHAR                                    := 'Y'
    ,p_log_level                    IN  XX_FND_LOGGER.gn_session_log_level%TYPE := 3
    )
IS
cv_proc_name            CONSTANT    VARCHAR2(30)                     := 'p_clean_dee_wferr';
cv_log_context          CONSTANT    VARCHAR2(2000)                   := lower(cv_package_name) || '.' || lower(cv_proc_name);
BEGIN -- p_clean_dee_wferr
    xx_fnd_logger.set_log_level(p_log_level);
    xx_fnd_logger.info(cv_log_context, 'd�but');
    xx_fnd_logger.info(cv_log_context, 'param�tre         p_item_type:'||p_item_type);
    xx_fnd_logger.info(cv_log_context, 'param�tre          p_item_key:'||p_item_key);
    xx_fnd_logger.info(cv_log_context, 'param�tre     p_root_activity:'||p_root_activity);
    xx_fnd_logger.info(cv_log_context, 'param�tre  p_parent_item_type:'||p_parent_item_type);
    xx_fnd_logger.info(cv_log_context, 'param�tre               p_age:'||p_age);
    xx_fnd_logger.info(cv_log_context, 'param�tre            p_commit:'||p_commit);
    xx_fnd_logger.debug(cv_log_context, 'param�tre         p_log_level:'||p_log_level);
    errbuf := NULL;
    retcode := TO_CHAR(fc_clean_dee_wferr(
                             p_commit           => p_commit
                            ,p_item_type        => p_item_type
                            ,p_item_key         => p_item_key
                            ,p_root_activity    => p_root_activity
                            ,p_parent_item_type => p_parent_item_type
                            ,p_age              => p_age
                            ,p_log_level        => p_log_level
                            )
                       );
    IF retcode = '0'
    THEN
        xx_fnd_logger.info(cv_log_context, 'fin normale');
        IF errbuf IS NULL THEN errbuf := null; END IF;
    ELSIF retcode = '1'
    THEN
        xx_fnd_logger.warning(cv_log_context, 'fin avertissement');
        IF errbuf IS NULL THEN errbuf := 'fin avertissement non d�taill�e'; END IF;
    ELSIF retcode = '2'
    THEN
        xx_fnd_logger.error(cv_log_context, 'fin erreur');
        IF errbuf IS NULL THEN errbuf := 'fin erreur non d�taill�e'; END IF;
    ELSE
        xx_fnd_logger.error(cv_log_context, 'fin inattendue');
        IF errbuf IS NULL THEN errbuf := 'fin inattendue non d�taill�e'; END IF;
    END IF;
EXCEPTION
    WHEN OTHERS THEN
          ROLLBACK; xx_fnd_logger.info(cv_log_context, 'ROLLBACK eff�cut�');
          retcode := '2';
          errbuf := SUBSTR(cv_log_context || ', erreur : '||SQLCODE||'-'||SQLERRM, 1, 2000);
          xx_fnd_logger.error(cv_log_context, 'fin erreur '||SUBSTR(cv_log_context || ', erreur : '||SQLCODE||'-'||SQLERRM, 1, 1000));
END p_clean_dee_wferr;
------------------------------------------------------------------------------------
-- NAME        : fc_hold_requests
----------------------------------------------------------------------------------------
FUNCTION fc_hold_requests(
     p_hold_curr                    IN  CHAR
    ,p_hold_new                     IN  CHAR
    ,p_updated_by_curr              IN  fnd_user.user_id%TYPE                   := NULL
    ,p_updated_by_new               IN  fnd_user.user_id%TYPE                   := NULL
    ,p_request_id                   IN  fnd_concurrent_requests.request_id%TYPE := NULL
    ,p_phase_code                   IN  fnd_concurrent_requests.phase_code%TYPE := 'P'
    ,p_requested_by                 IN  fnd_user.user_id%TYPE                   := NULL
    ,p_commit                       IN  CHAR                                    := 'Y'
    ,p_log_level                    IN  XX_FND_LOGGER.gn_session_log_level%TYPE := 3
    )
RETURN NUMBER
IS
    PRAGMA AUTONOMOUS_TRANSACTION;
cv_proc_name            CONSTANT    VARCHAR2(30)                     := 'fc_hold_requests';
cv_log_context          CONSTANT    VARCHAR2(2000)                   := lower(cv_package_name) || '.' || lower(cv_proc_name);
v_nbConcReq_updated                 NUMBER;
CURSOR c_concReq (
             p_request_id                   IN  fnd_concurrent_requests.request_id%TYPE
            ,p_phase_code                   IN  fnd_concurrent_requests.phase_code%TYPE := 'P'
            ,p_requested_by                 IN  fnd_user.user_id%TYPE
            ,p_updated_by_2process          IN  fnd_user.user_id%TYPE
            ,p_hold_2process                IN  CHAR
            )
            IS
    SELECT db.name                  dbName
        ,cr.request_id
        ,cr.responsibility_id
        ,cr.hold_flag
        ,cr.phase_code
        ,cr.status_code
        ,cr.actual_start_date
        ,cr.request_date
        ,cr.requested_start_date
        ,cr.requested_by
        ,cr.last_updated_by
    FROM v$database db
        ,fnd_concurrent_requests    cr
    WHERE 1=1
    AND cr.request_id                       = COALESCE(p_request_id, cr.request_id)
    AND cr.phase_code                       = COALESCE(p_phase_code, cr.phase_code)
    AND cr.requested_by                     = COALESCE(p_requested_by, cr.requested_by)
    AND cr.last_updated_by                  = COALESCE(p_updated_by_2process, cr.last_updated_by)
    AND cr.hold_flag                        = p_hold_2process
    and cr.requested_by                     > 0
    ;
BEGIN -- fc_hold_requests
    xx_fnd_logger.set_log_level(p_log_level);
    xx_fnd_logger.info(cv_log_context, 'start');
    xx_fnd_logger.info(cv_log_context, 'param�tre         p_hold_curr:'||p_hold_curr);
    xx_fnd_logger.info(cv_log_context, 'param�tre          p_hold_new:'||p_hold_new);
    xx_fnd_logger.info(cv_log_context, 'param�tre        p_request_id:'||p_request_id);
    xx_fnd_logger.info(cv_log_context, 'param�tre        p_phase_code:'||p_phase_code);
    xx_fnd_logger.info(cv_log_context, 'param�tre      p_requested_by:'||p_requested_by);
    xx_fnd_logger.info(cv_log_context, 'param�tre   p_updated_by_curr:'||p_updated_by_curr);
    xx_fnd_logger.info(cv_log_context, 'param�tre    p_updated_by_new:'||p_updated_by_new);
    xx_fnd_logger.info(cv_log_context, 'param�tre            p_commit:'||p_commit);
    xx_fnd_logger.debug(cv_log_context, 'parametre         p_log_level:'||p_log_level);
    v_nbConcReq_updated := 0;

    --
    -- Check parameters--
    --
    --if p_hold in ('Y')
    --and p_updated_by IS NULL
    --than raise E_updated_by_mandatory
    --
    -- loop de traitement des requests
    --
    FOR r_concReq IN c_concReq(
             p_request_id           => p_request_id
            ,p_phase_code           => p_phase_code
            ,p_requested_by         => p_requested_by
            ,p_updated_by_2process  => p_updated_by_curr
            ,p_hold_2process        => p_hold_curr
            )
    LOOP
        xx_fnd_logger.trace(cv_log_context, 'request_id:'||r_concReq.request_id, 'STEP');
        xx_fnd_logger.trace(cv_log_context, 'request_date:'||to_char(r_concReq.request_date,'dd/mm/yyyy hh24:mi'));
        xx_fnd_logger.trace(cv_log_context, 'responsibility_id:'||r_concReq.responsibility_id);
        xx_fnd_logger.trace(cv_log_context, 'hold_flag:'||r_concReq.hold_flag);
        xx_fnd_logger.trace(cv_log_context, 'phase_code:'||r_concReq.phase_code);
        xx_fnd_logger.trace(cv_log_context, 'requested_by:'||r_concReq.requested_by);
        xx_fnd_logger.trace(cv_log_context, 'last_updated_by:'||r_concReq.last_updated_by);
        xx_fnd_logger.trace(cv_log_context, 'status_code:'||r_concReq.status_code);
        xx_fnd_logger.trace(cv_log_context, 'requested_start_date:'||to_char(r_concReq.requested_start_date,'dd/mm/yyyy hh24:mi'));
        xx_fnd_logger.trace(cv_log_context, 'actual_start_date:'||to_char(r_concReq.actual_start_date,'dd/mm/yyyy hh24:mi'));
        update fnd_concurrent_requests cr
            set cr.hold_flag = p_hold_new, cr.last_updated_by=COALESCE(p_updated_by_new, cr.last_updated_by)
            where 1=1
            and cr.request_id = r_concReq.request_id
            and cr.phase_code = r_concReq.phase_code
            and cr.requested_by > 0
            and cr.hold_flag <> p_hold_new
            ;
        v_nbConcReq_updated := v_nbConcReq_updated + SQL%ROWCOUNT;
        IF COALESCE(SQL%ROWCOUNT,0) <> 0
        THEN
            xx_fnd_logger.debug(cv_log_context, 'request_id:'||r_concReq.request_id||' updated '||SQL%ROWCOUNT);
        END IF;
    END LOOP; -- c_concReq
    xx_fnd_logger.info(cv_log_context, 'nb fnd_concurrent_requests updated:'||v_nbConcReq_updated);
    --
    IF UPPER(substr(p_commit,1,1)) IN ('Y','O') THEN COMMIT; xx_fnd_logger.info(cv_log_context, 'COMMIT eff�cut�'); ELSE ROLLBACK; xx_fnd_logger.info(cv_log_context, 'ROLLBACK eff�cut�'); END IF;
    xx_fnd_logger.info(cv_log_context, 'fin normale');
    RETURN 0;
EXCEPTION
WHEN OTHERS THEN
    xx_fnd_logger.error(cv_log_context, 'erreur non trait�e');
    ROLLBACK; xx_fnd_logger.info(cv_log_context, 'ROLLBACK eff�cut�');
    RAISE; -- exception non trait�e (la procedure appelant plantera si aucun traitement de cette exception)
END fc_hold_requests;
PROCEDURE p_hold_requests(
     errbuf                         OUT VARCHAR2
    ,retcode                        OUT VARCHAR2
    ,p_hold_curr                    IN  CHAR
    ,p_hold_new                     IN  CHAR
    ,p_updated_by_curr              IN  fnd_user.user_id%TYPE                   := NULL
    ,p_updated_by_new               IN  fnd_user.user_id%TYPE                   := NULL
    ,p_request_id                   IN  fnd_concurrent_requests.request_id%TYPE := NULL
    ,p_phase_code                   IN  fnd_concurrent_requests.phase_code%TYPE := 'P'
    ,p_requested_by                 IN  fnd_user.user_id%TYPE                   := NULL
    ,p_commit                       IN  CHAR                                    := 'Y'
    ,p_log_level                    IN  XX_FND_LOGGER.gn_session_log_level%TYPE := 3
    )
IS
cv_proc_name            CONSTANT    VARCHAR2(30)                     := 'p_hold_requests';
cv_log_context          CONSTANT    VARCHAR2(2000)                   := lower(cv_package_name) || '.' || lower(cv_proc_name);
BEGIN -- p_hold_requests
    xx_fnd_logger.set_log_level(p_log_level);
    xx_fnd_logger.info(cv_log_context, 'd�but');
    xx_fnd_logger.info(cv_log_context, 'param�tre         p_hold_curr:'||p_hold_curr);
    xx_fnd_logger.info(cv_log_context, 'param�tre          p_hold_new:'||p_hold_new);
    xx_fnd_logger.info(cv_log_context, 'param�tre        p_request_id:'||p_request_id);
    xx_fnd_logger.info(cv_log_context, 'param�tre        p_phase_code:'||p_phase_code);
    xx_fnd_logger.info(cv_log_context, 'param�tre      p_requested_by:'||p_requested_by);
    xx_fnd_logger.info(cv_log_context, 'param�tre   p_updated_by_curr:'||p_updated_by_curr);
    xx_fnd_logger.info(cv_log_context, 'param�tre    p_updated_by_new:'||p_updated_by_new);
    xx_fnd_logger.info(cv_log_context, 'param�tre            p_commit:'||p_commit);
    xx_fnd_logger.debug(cv_log_context, 'parametre         p_log_level:'||p_log_level);
    errbuf := NULL;
    retcode := TO_CHAR(fc_hold_requests(
                             p_commit           => p_commit
                            ,p_hold_curr        => p_hold_curr
                            ,p_hold_new         => p_hold_new
                            ,p_request_id       => p_request_id
                            ,p_phase_code       => p_phase_code
                            ,p_requested_by     => p_requested_by
                            ,p_updated_by_curr  => p_updated_by_curr
                            ,p_updated_by_new   => p_updated_by_new
                            ,p_log_level        => p_log_level
                            )
                       );
    IF retcode = '0'
    THEN
        xx_fnd_logger.info(cv_log_context, 'fin normale');
        IF errbuf IS NULL THEN errbuf := null; END IF;
    ELSIF retcode = '1'
    THEN
        xx_fnd_logger.warning(cv_log_context, 'fin avertissement');
        IF errbuf IS NULL THEN errbuf := 'fin avertissement non d�taill�e'; END IF;
    ELSIF retcode = '2'
    THEN
        xx_fnd_logger.error(cv_log_context, 'fin erreur');
        IF errbuf IS NULL THEN errbuf := 'fin erreur non d�taill�e'; END IF;
    ELSE
        xx_fnd_logger.error(cv_log_context, 'fin inattendue');
        IF errbuf IS NULL THEN errbuf := 'fin inattendue non d�taill�e'; END IF;
    END IF;
EXCEPTION
    WHEN OTHERS THEN
          ROLLBACK; xx_fnd_logger.info(cv_log_context, 'ROLLBACK eff�cut�');
          retcode := '2';
          errbuf := SUBSTR(cv_log_context || ', erreur : '||SQLCODE||'-'||SQLERRM, 1, 2000);
          xx_fnd_logger.error(cv_log_context, 'fin erreur '||SUBSTR(cv_log_context || ', erreur : '||SQLCODE||'-'||SQLERRM, 1, 1000));
END p_hold_requests;
--#XX V12.2.9.01.e.n.
END XXCFI_UTILS_PKG;
/
SHOW ERROR

EXIT;